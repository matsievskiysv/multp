module EMgeometry

using StaticArrays, SparseArrays

import Base: +, -, *, /, <, <=, show, getindex, size, length, eltype, convert, promote_rule, isapprox, Module

export SPoint, MPoint, SField, MField, Cell, interpolation_coeffs, interpolate, MeshStep, Mesh, Mesh,
    process, intersection, Intersection, AbstractBoundary, Intersection, MeshStepIrregular, Segment, SegmentCache,
    Consume, collision!, Bounce, Pozitron, rotate, rotate!, SecEmissionWall, SecEmissionFilm

# <<< Types

const SPoint = SVector{N, T} where {N, T<:Number}
const MPoint = MVector{N, T} where {N, T<:Number}
const AnyPoint = Union{SPoint{N, T}, MPoint{N, T}} where {N, T<:Number}

SPoint(x::AnyPoint{N, T}) where {N, T<:Number} = SVector{N, T}(x)
SPoint(x::T) where T<:Number = SPoint{1, T}(x)
SPoint(x::T, y::T) where T<:Number = SPoint{2, T}(convert(T, x), convert(T, y))
SPoint(x, y) = SPoint(promote(x, y)...)
SPoint(x::T, y::T, z::T) where T<:Number = SPoint{3, T}(x, y, z)
SPoint(x, y, z) = SPoint(promote(x, y, z)...)

MPoint(x::AnyPoint{N, T}) where {N, T<:Number} = MVector{N, T}(x)
MPoint(x::T) where T<:Number = MPoint{1, T}(x)
MPoint(x::T, y::T) where {T} = MPoint{2, T}(x, y)
MPoint(x, y) = MPoint(promote(x, y)...)
MPoint(x::T, y::T, z::T) where {T} = MPoint{3, T}(x, y, z)
MPoint(x, y, z) = MPoint(promote(x, y, z)...)

Base.convert(::Type{SPoint}, x::AnyPoint{N, T}) where {N, T<:Number} = SPoint{N, T}(x)
Base.convert(::Type{MPoint}, x::AnyPoint{N, T}) where {N, T<:Number} = MPoint{N, T}(x)

abstract type AbstractField{L, N, Tl<:Number, Tn<:Number} end

struct SField{L, N, Tl, Tn} <: AbstractField{L, N, Tl, Tn}
    field::SPoint{L, Tl}
    point::SPoint{N, Tn}
    SField{L, N, Tl, Tn}(x::AnyPoint{L, Tl}, y::AnyPoint{N, Tn}) where {L, N, Tl<:Number, Tn<:Number} =
        L > N ? throw(DomainError(L, "Field dimension L must be less or equal to spatial dimension N.")) :
        new{L, N, Tl, Tn}(SPoint{L, Tl}(x), SPoint{N, Tn}(y))
end

SField(x::AnyPoint{L, Tl}, y::AnyPoint{N, Tn}) where {L, N, Tl<:Number, Tn<:Number} = SField{L, N, Tl, Tn}(x, y)

SField(x::AbstractField{L, N, Tl, Tn}) where {L, N, Tl<:Number, Tn<:Number} = SField{L, N, Tl, Tn}(x[1], x[2])

mutable struct MField{L, N, Tl, Tn<:Number} <: AbstractField{L, N, Tl, Tn}
    field::MPoint{L, Tl}
    point::MPoint{N, Tn}
    MField{L, N, Tl, Tn}(x::AnyPoint{L, Tl}, y::AnyPoint{N, Tn}) where {L, N, Tl<:Number, Tn<:Number} =
        L > N ? throw(DomainError(L, "Field dimension L must be less or equal to spatial dimension N.")) :
        new{L, N, Tl, Tn}(MPoint{L, Tl}(x), MPoint{N, Tn}(y))
end

MField(x::AnyPoint{L, Tl}, y::AnyPoint{N, Tn}) where {L, N, Tl<:Number, Tn<:Number} = MField{L, N, Tl, Tn}(x, y)

MField(x::AbstractField{L, N, Tl, Tn}) where {L, N, Tl<:Number, Tn<:Number} = MField{L, N, Tl, Tn}(x[1], x[2])

const AnyField = Union{SField{L, N, Tl, Tn}, MField{L, N, Tl, Tn}} where {L, N, Tl<:Number, Tn<:Number}

Base.show(io::IO, z::AbstractField{N, L}) where {L, N, Tl<:Number, Tn<:Number} =
    print(io, "Field:\n\tfield $(L)D: $(z.field)\n\tpoint $(N)D: $(z.point)")

function Base.getindex(z::AbstractField, n)
    if n == 1
        return z.field
    elseif n == 2
        return z.point
    else
        throw(BoundsError(n, "Allowed indexes are 1 and 2."))
    end
end

Base.convert(::Type{SField}, x::AbstractField{L, N, Tl, Tn}) where {L, N, Tl<:Number, Tn<:Number} = SField(x)

Base.convert(::Type{MField}, x::AbstractField{L, N, Tl, Tn}) where {L, N, Tl<:Number, Tn<:Number} = MField(x)

function Base.setindex!(x::MField{L, N, Tl, Tn}, data, n) where {L, N, Tl<:Number, Tn<:Number}
    if n == 1
        z.field == data
    elseif n == 2
        z.point == data
    else
        throw(BoundsError(n, "Allowed indexes are 1 and 2."))
    end
end

Base.eltype(z::AbstractField) = (eltype(z.field), eltype(z.point))

# >>>

# <<< Basic methods

<(a::AnyPoint{1, T}, b::AnyPoint{1, T}) where T<:Number = a[1] < b[1]
<(a::AnyPoint{2, T}, b::AnyPoint{2, T}) where T<:Number =
    if (a[2] == b[2])
        a[1] < b[1]
    else
        a[2] < b[2]
    end
<(a::AnyPoint{3, T}, b::AnyPoint{3, T}) where T<:Number =
    if (a[3] == b[3])
        if (a[2] == b[2])
            a[1] < b[1]
        else
            a[2] < b[2]
        end
    else
        a[3] < b[3]
    end

<=(a::AnyPoint{1, T}, b::AnyPoint{1, T}) where T<:Number = a[1] <= b[1]
<=(a::AnyPoint{2, T}, b::AnyPoint{2, T}) where T<:Number =
    if (a[2] == b[2])
        a[1] <= b[1]
    else
        a[2] <= b[2]
    end
<=(a::AnyPoint{3, T}, b::AnyPoint{3, T}) where T<:Number =
    if (a[3] == b[3])
        if (a[2] == b[2])
            a[1] <= b[1]
        else
            a[2] <= b[2]
        end
    else
        a[3] <= b[3]
    end

<(a::AbstractField, b::AbstractField) = Module(a) < Module(b)
<=(a::AbstractField, b::AbstractField) = Module(a) <= Module(b)

+(a::AbstractField, b::AbstractField) = a + b.field
-(a::AbstractField, b::AbstractField) = a - b.field

+(a::SField, b::AnyPoint) = SField(a[1] .+ SPoint(b), a[2])
-(a::SField, b::AnyPoint) = SField(a[1] .- SPoint(b), a[2])

function +(a::MField, b::AnyPoint)
    a.field .+= MPoint(b)
    return a
end

function -(a::MField, b::AnyPoint)
    a.field .-= MPoint(b)
    return a
end

+(a::SField, b::T) where T<:Number = SField(a[1] .+ b, a[2])
-(a::SField, b::T) where T<:Number = SField(a[1] .- b, a[2])

function +(a::MField, b::T) where T<:Number
    a.field .+= b
    return a
end

function -(a::MField, b::T) where T<:Number
    a.field .-= b
    return a
end

Module(a::AnyPoint) = sqrt(sum(a.^2))

Module(a::AbstractField; field::Bool=true) = field ? Module(a.field) : Module(a.point)

isapprox(a::AbstractField, b::AbstractField; kwargs...) =
    isapprox(a.point, b.point; kwargs...) | isapprox(a.field, b.field; kwargs...)

function rotate(a::AnyPoint{1, T}, α::Number) where T<:Number
    return a * cos(α)
end

function rotate!(a::MPoint{1, T}, α::Number) where T<:Number
    a[1] *= cos(α)
end

function rotate(a::SPoint{2, T}, α::Number) where T<:Number
    return SPoint(SMatrix{2, 2}(cos(α), sin(α), -sin(α), cos(α)) * a)
end

function rotate(a::MPoint{2, T}, α::Number) where T<:Number
    return MPoint(SMatrix{2, 2}(cos(α), sin(α), -sin(α), cos(α)) * a)
end

function rotate!(a::MPoint{2, T}, α::Number) where T<:Number
    p = SMatrix{2, 2}(cos(α), sin(α), -sin(α), cos(α)) * a
    a[1] = p[1]
    a[2] = p[2]
end

function rotate(a::SPoint{3, T}; α::Number=0, β::Number=0, γ::Number=0) where T<:Number
    return SPoint(SMatrix{3, 3}(cos(γ), sin(γ), 0, -sin(γ), cos(γ), 0, 0, 0, 1) *
                  SMatrix{3, 3}(cos(β), 0, -sin(β), 0, 1, 0, sin(β), 0, cos(β)) *
                  SMatrix{3, 3}(1, 0, 0, 0, cos(α), sin(α), 0, -sin(α), cos(α)) *
                  a)
end

function rotate(a::MPoint{3, T}; α::Number=0, β::Number=0, γ::Number=0) where T<:Number
    return MPoint(SMatrix{3, 3}(cos(γ), sin(γ), 0, -sin(γ), cos(γ), 0, 0, 0, 1) *
                  SMatrix{3, 3}(cos(β), 0, -sin(β), 0, 1, 0, sin(β), 0, cos(β)) *
                  SMatrix{3, 3}(1, 0, 0, 0, cos(α), sin(α), 0, -sin(α), cos(α)) *
                  a)
end

function rotate!(a::MPoint{3, T}; α::Number=0, β::Number=0, γ::Number=0) where T<:Number
    p = (SMatrix{3, 3}(cos(γ), sin(γ), 0, -sin(γ), cos(γ), 0, 0, 0, 1) *
         SMatrix{3, 3}(cos(β), 0, -sin(β), 0, 1, 0, sin(β), 0, cos(β)) *
         SMatrix{3, 3}(1, 0, 0, 0, cos(α), sin(α), 0, -sin(α), cos(α)) *
         a)
    a[1] = p[1]
    a[2] = p[2]
    a[3] = p[3]
end

function _rotate_field(a::SField, α::Number)
    return SField(rotate(a.field, α), a.point)
end

function _rotate_point(a::SField, α::Number)
    return SField(a.field, rotate(a.point, α))
end

function _rotate_field(a::MField, α::Number)
    return MField(rotate(a.field, α), a.point)
end

function _rotate_point(a::MField, α::Number)
    return MField(a.field, rotate(a.point, α))
end

function rotate(a::AnyField, α::Number; field::Bool=false)
    if field
        return _rotate_field(a, α)
    else
        return _rotate_point(a, α)
    end
end

function _rotate_field!(a::MField, α::Number)
    rotate!(a.field, α)
end

function _rotate_point!(a::MField, α::Number)
    rotate!(a.point, α)
end

function rotate!(a::MField, α::Number; field::Bool=false)
    if field
        _rotate_field!(a, α)
    else
        _rotate_point!(a, α)
    end
end

function _rotate_field(a::SField; α::Number=0, β::Number=0, γ::Number=0)
    return SField(rotate(a.field; α=α, β=β, γ=γ), a.point)
end

function _rotate_point(a::SField; α::Number=0, β::Number=0, γ::Number=0)
    return SField(a.field, rotate(a.point; α=α, β=β, γ=γ))
end

function _rotate_field(a::MField; α::Number=0, β::Number=0, γ::Number=0)
    return MField(rotate(a.field; α=α, β=β, γ=γ), a.point)
end

function _rotate_point(a::MField; α::Number=0, β::Number=0, γ::Number=0)
    return MField(a.field, rotate(a.point; α=α, β=β, γ=γ))
end

function rotate(a::AnyField; α::Number=0, β::Number=0, γ::Number=0, field::Bool=false)
    if field
        return _rotate_field(a; α=α, β=β, γ=γ)
    else
        return _rotate_point(a; α=α, β=β, γ=γ)
    end
end

function _rotate_field!(a::MField; α::Number=0, β::Number=0, γ::Number=0)
    rotate!(a.field; α=α, β=β, γ=γ)
end

function _rotate_point!(a::MField; α::Number=0, β::Number=0, γ::Number=0)
    rotate!(a.point; α=α, β=β, γ=γ)
end

function rotate!(a::MField; α::Number=0, β::Number=0, γ::Number=0, field::Bool=false)
    if field
        _rotate_field!(a; α=α, β=β, γ=γ)
    else
        _rotate_point!(a; α=α, β=β, γ=γ)
    end
end

# >>>

# <<< Cells

# abstract type AbstractCell{N, L} end

# Base.getindex(x::AbstractCell, n) = x.points[n]

# struct Cell1D{L} <: AbstractCell{1, L}
#     points::SVector{2, AbstractField{1, L}}
# end

# Base.show(io::IO, z::Cell1D) =
#     print(io, "Mesh cell 1D:\npoint 1: $(z.points[1])\n",
#           "point 2: $(z.points[2])")

# Cell(a::AbstractField{1, 1},
#      b::AbstractField{1, 1}) =
#          Cell1D{1}(SVector{2}(sort([a, b],
#                                    lt=(x, y) -> x[2] < y[2])))

# struct Cell2D{L} <: AbstractCell{2, L}
#     points::SVector{4, AbstractField{2, L}}
# end

# Base.show(io::IO, z::Cell2D) =
#     print(io, "Mesh cell 2D:\n",
#           "point 1: $(z.points[1])\n",
#           "point 2: $(z.points[2])\n",
#           "point 3: $(z.points[3])\n",
#           "point 4: $(z.points[4])")

# Cell(a::AbstractField{2, L},
#      b::AbstractField{2, L},
#      c::AbstractField{2, L},
#      d::AbstractField{2, L}) where {L} =
#          Cell2D{L}(SVector{4}(sort([a, b, c, d],
#                                    lt=(x, y) -> x[2] < y[2])))

# struct Cell3D{L} <: AbstractCell{3, L}
#     points::SVector{8, AbstractField{3, L}}
# end

# Base.show(io::IO, z::Cell3D) =
#     print(io, "Mesh cell 3D:\n",
#           "point 1: $(z.points[1])\n",
#           "point 2: $(z.points[2])\n",
#           "point 3: $(z.points[3])\n",
#           "point 4: $(z.points[4])\n",
#           "point 5: $(z.points[5])\n",
#           "point 6: $(z.points[6])\n",
#           "point 7: $(z.points[7])\n",
#           "point 8: $(z.points[8])")

# Cell(a::AbstractField{3, L},
#      b::AbstractField{3, L},
#      c::AbstractField{3, L},
#      d::AbstractField{3, L},
#      e::AbstractField{3, L},
#      f::AbstractField{3, L},
#      g::AbstractField{3, L},
#      h::AbstractField{3, L}) where L =
#          Cell3D{L}(SVector{8}(sort([a, b, c, d, e, f, g, h],
#                                    lt=(x, y) -> x.point < y.point)))

# Base.getindex(a::AbstractCell, n) = a.points[n]

# # >>>

# # <<< Interpolation

# abstract type AbstractInterpolation{N, L} end

# struct Interpolation_coeffs1D{L} <: AbstractInterpolation{1, L}
#     value::NTuple{L, SPoint{2}}
# end

# struct Interpolation_coeffs2D{L} <: AbstractInterpolation{2, L}
#     value::NTuple{L, SPoint{4}}
# end

# struct Interpolation_coeffs3D{L} <: AbstractInterpolation{3, L}
#     value::NTuple{L, SVector{8}}
# end

# function interpolation_coeffs(cell::Cell1D{L}) where L
#     return Interpolation_coeffs1D(
#         Tuple([SMatrix{2, 2}(cell[1].point[1],
#                              cell[2].point[1],
#                              1, 1) \
#                SVector{2}(cell[1].value[i],
#                           cell[2].value[i]) for i in 1:L]))
# end

# function interpolation_coeffs(cell::Cell2D{L, T}) where {L, T}
#     return Interpolation_coeffs2D(
#         Tuple([SMatrix{4, 4}(cell[1].point[1] * cell[1].point[2],
#                              cell[2].point[1] * cell[2].point[2],
#                              cell[3].point[1] * cell[3].point[2],
#                              cell[4].point[1] * cell[4].point[2],
#                              cell[1].point[2],
#                              cell[2].point[2],
#                              cell[3].point[2],
#                              cell[4].point[2],
#                              cell[1].point[1],
#                              cell[2].point[1],
#                              cell[3].point[1],
#                              cell[4].point[1],
#                              1, 1, 1, 1) \
#                SVector{4}(
#                    cell[1].value[i],
#                    cell[2].value[i],
#                    cell[3].value[i],
#                    cell[4].value[i]) for i in 1:L]))
# end

# function interpolation_coeffs(cell::Cell3D{L, T}) where {L, T}
#     return Interpolation_coeffs3D(
#         Tuple([SMatrix{8, 8}(cell[1].point[1] * cell[1].point[2] * cell[1].point[3],
#                              cell[2].point[1] * cell[2].point[2] * cell[2].point[3],
#                              cell[3].point[1] * cell[3].point[2] * cell[3].point[3],
#                              cell[4].point[1] * cell[4].point[2] * cell[4].point[3],
#                              cell[5].point[1] * cell[5].point[2] * cell[5].point[3],
#                              cell[6].point[1] * cell[6].point[2] * cell[6].point[3],
#                              cell[7].point[1] * cell[7].point[2] * cell[7].point[3],
#                              cell[8].point[1] * cell[8].point[2] * cell[8].point[3],
#                              cell[1].point[2] * cell[1].point[3],
#                              cell[2].point[2] * cell[2].point[3],
#                              cell[3].point[2] * cell[3].point[3],
#                              cell[4].point[2] * cell[4].point[3],
#                              cell[5].point[2] * cell[5].point[3],
#                              cell[6].point[2] * cell[6].point[3],
#                              cell[7].point[2] * cell[7].point[3],
#                              cell[8].point[2] * cell[8].point[3],
#                              cell[1].point[1] * cell[1].point[3],
#                              cell[2].point[1] * cell[2].point[3],
#                              cell[3].point[1] * cell[3].point[3],
#                              cell[4].point[1] * cell[4].point[3],
#                              cell[5].point[1] * cell[5].point[3],
#                              cell[6].point[1] * cell[6].point[3],
#                              cell[7].point[1] * cell[7].point[3],
#                              cell[8].point[1] * cell[8].point[3],
#                              cell[1].point[1] * cell[1].point[2],
#                              cell[2].point[1] * cell[2].point[2],
#                              cell[3].point[1] * cell[3].point[2],
#                              cell[4].point[1] * cell[4].point[2],
#                              cell[5].point[1] * cell[5].point[2],
#                              cell[6].point[1] * cell[6].point[2],
#                              cell[7].point[1] * cell[7].point[2],
#                              cell[8].point[1] * cell[8].point[2],
#                              cell[1].point[3],
#                              cell[2].point[3],
#                              cell[3].point[3],
#                              cell[4].point[3],
#                              cell[5].point[3],
#                              cell[6].point[3],
#                              cell[7].point[3],
#                              cell[8].point[3],
#                              cell[1].point[2],
#                              cell[2].point[2],
#                              cell[3].point[2],
#                              cell[4].point[2],
#                              cell[5].point[2],
#                              cell[6].point[2],
#                              cell[7].point[2],
#                              cell[8].point[2],
#                              cell[1].point[1],
#                              cell[2].point[1],
#                              cell[3].point[1],
#                              cell[4].point[1],
#                              cell[5].point[1],
#                              cell[6].point[1],
#                              cell[7].point[1],
#                              cell[8].point[1],
#                              1, 1, 1, 1, 1, 1, 1, 1) \
#                SVector{8}(
#                    cell[1].value[i],
#                    cell[2].value[i],
#                    cell[3].value[i],
#                    cell[4].value[i],
#                    cell[5].value[i],
#                    cell[6].value[i],
#                    cell[7].value[i],
#                    cell[8].value[i]) for i in 1:L]))
# end

# function interpolate(x::SPoint{1, T},
#                      coeffs::Interpolation_coeffs1D{L, T}) where {L, T}
#     return SField(SPoint{L}([sum(SVector(x[1], 1) .* coeffs.value[i]) for i in 1:L]), x)
# end

# function interpolate(x::MPoint{1, T},
#                      coeffs::Interpolation_coeffs1D{L, T}) where {L, T}
#     return MField(MPoint{L}([sum(MVector(x[1], 1) .* coeffs.value[i]) for i in 1:L]), x)
# end

# function interpolate(x::SPoint{2, T},
#                      coeffs::Interpolation_coeffs2D{L, T}) where {L, T}
#     return SField(SPoint{L}([sum(SVector(x[1] * x[2], x[2], x[1], 1) .* coeffs.value[i]) for i in 1:L]), x)
# end

# function interpolate(x::MPoint{2, T},
#                      coeffs::Interpolation_coeffs2D{L, T}) where {L, T}
#     return MField(MPoint{L}([sum(MVector(x[1] * x[2], x[2], x[1], 1) .* coeffs.value[i]) for i in 1:L]), x)
# end

# function interpolate(x::SPoint{3, T},
#                      coeffs::Interpolation_coeffs3D{L, T}) where {L, T}
#     return SField(SPoint{L}([sum(SVector(x[1] * x[2] * x[3], x[2] * x[3],
#                                          x[1] * x[3], x[1] * x[2],
#                                          x[3], x[2], x[1], 1) .* coeffs.value[i]) for i in 1:L]), x)
# end

# function interpolate(x::MPoint{3, T},
#                      coeffs::Interpolation_coeffs3D{L, T}) where {L, T}
#     return MField(MPoint{L}([sum(MVector(x[1] * x[2] * x[3], x[2] * x[3],
#                                          x[1] * x[3], x[1] * x[2],
#                                          x[3], x[2], x[1], 1) .* coeffs.value[i]) for i in 1:L]), x)
# end

# function interpolate(x::AnyPoint, cell::AbstractCell)
#     return interpolate(x, interpolation_coeffs(cell))
# end

# # >>>

# # <<< Mesh meta

# struct MeshStepRegular{T}
#     step::T
# end

# struct MeshStepIrregular{T}
#     step::Vector{T}
# end

# const AnyMeshStep = Union{MeshStepRegular{T}, MeshStepIrregular{T}} where {T}

# _MeshStep(x) = MeshStepRegular(x)

# _MeshStep(x::Vector) = MeshStepIrregular(x)

# struct MeshStep{N, T}
#     steps::SVector{N, AnyMeshStep{T}}
#     MeshStep(x::Vararg{Union{T, Vector{T}}, N}) where {N, T} = new{N, T}(SVector{N}(_MeshStep.(x)))
# end

# Base.getindex(a::MeshStep, n) = a.steps[n]

# Base.size(a::MeshStep{N, T}) where {N, T} = (N,)

# Base.length(a::MeshStep) = prod(size(a))

# Base.show(io::IO, z::MeshStep{N, T}) where {N,  T} =
#     print(io, "MeshStep$(N)D{$T}:\n\t$([s.step for s in z.steps])")

# struct Mesh{N, Ts, Tm}
#     step::MeshStep{N, Ts}
#     mesh::Array{Tm, N}
#     function Mesh(x::MeshStep{N, Ts}, y::NTuple{N, Integer}, ::Type{Tm}) where {N, Ts, Tm}
#         for d in 1:length(x)
#             if (typeof(x[d]) <: MeshStepIrregular) && (length(x[d].step) != y[d])
#                 error("Dimension mismatch")
#             end
#         end
#         new{N, Ts, Tm}(x, Array{Tm}(undef, y))
#     end
# end

# Base.show(io::IO, z::Mesh{N, Ts, Tm}) where {N, Ts, Tm} =
#     print(io, "Mesh$(N)D{$Tm}:\n\t$(z.step)")

# >>>

# <<< Collision

struct Intersection{N, T}
    point::SPoint{N, T}
    normal::SPoint{N, T}
    Intersection{N, T}(x::AnyPoint{N, T}, y::AnyPoint{N, T}) where {N, T<:Number} =
        new{N, T}(SPoint(x), SPoint(y))
end

Intersection(x::AnyPoint{N, T}, y::AnyPoint{N, T}) where {N, T<:Number} =
    Intersection{N, T}(SPoint(x), SPoint(y))

abstract type AbstractParticle{N, T<:Number} end

# struct NoParticle{N, T} <: AbstractParticle{N, T} end

# NoParticle(par::AbstractParticle{N, T}) where {N, T<:Number} = NoParticle{N, T}()

mutable struct Pozitron{N, T} <: AbstractParticle{N, T}
    position::MPoint{N, T}
    speed::MPoint{N, T}
    timeshift::T
    Pozitron{N, T}(pos::AnyPoint{N, T}, spd::AnyPoint{N, T}, delay) where {N, T<:Number} =
        new{N, T}(MPoint(pos), MPoint(spd), convert(T, delay))
end

Pozitron(pos::AnyPoint{N, T}, spd::AnyPoint{N, T}, delay::T) where {N, T<:Number} =
    Pozitron{N, T}(pos, spd, delay)

Pozitron(pos::AnyPoint{N, T}, spd::AnyPoint{N, T}) where {N, T<:Number} =
    Pozitron{N, T}(pos, spd, 0)

abstract type AbstractCollision{N, T} end

function collision!(particle::AbstractParticle{N, T},
                    time::T,
                    intersection::Intersection{N, T},
                    collision::AbstractCollision{N, T})::Vector{AbstractParticle{N, T}} where {N, T}
end

struct Consume{N, T} <: AbstractCollision{N, T} end

Consume(p::AbstractParticle{N, T}) where {N, T<:Number} = Consume{N, T}()

function collision!(particle::AbstractParticle{N, T},
                    time::T,
                    intersection::Intersection{N, T},
                    collision::Consume{N, T}) where {N, T<:Number}
    particle.speed .= typemax(T)
    particle.position .= intersection.point
    return []
end

struct Bounce{N, T} <: AbstractCollision{N, T}
    Bounce{N, T}() where {N, T} = new{N, T}()
end

Bounce(p::AbstractParticle{N, T}) where {N, T<:Number} = Bounce{N, T}()

function collision!(particle::AbstractParticle{1, T},
                    time::T,
                    intersection::Intersection{1, T},
                    collision::Bounce{1, T}) where T<:Number
    particle.timeshift = time - abs((intersection.point[1] -
                                     particle.position[1]) / particle.speed[1])
    particle.speed .= Module(particle.speed) * intersection.normal
    particle.position .= intersection.point
    return []
end

struct SecEmissionWall{N, T} <: AbstractCollision{N, T}
    yield::Function
    spawn::Function
    SecEmissionWall{N, T}(yield::Function, spawn::Function) where {N, T} = new{N, T}(yield, spawn)
end

SecEmissionWall(p::AbstractParticle{N, T},
                yield::Function,
                spawn::Function) where {N, T<:Number} = SecEmissionWall{N, T}(yield, spawn)

function collision!(particle::AbstractParticle{1, T},
                    time::T,
                    intersection::Intersection{1, T},
                    collision::SecEmissionWall{1, T}) where T<:Number
    speed = particle.speed
    normal = intersection.normal
    partnum = collision.yield(speed, 0)
    particle.timeshift = time - abs((intersection.point[1] -
                                     particle.position[1]) / particle.speed[1])
    particle.position .= MPoint(intersection.point)
    if partnum == 0
        particle.speed .= typemax(T)  # drop particle
    else
        particle.speed .= collision.spawn(speed, normal)
        partnum -= 1
        if partnum >= 1
            new_particles = Array{typeof(particle)}(undef, partnum)
            for i in 1:partnum
                new_particles[i] = deepcopy(particle)
                new_particles[i].speed .= collision.spawn(speed, normal)
            end
            return new_particles
        end
    end
    return []
end

struct SecEmissionFilm{N, T} <: AbstractCollision{N, T}
    yield_through::Function
    yield_back::Function
    spawn_through::Function
    spawn_back::Function
    SecEmissionFilm{N, T}(yield_through::Function, yield_back::Function,
                          spawn_through::Function, spawn_back::Function) where {N, T} =
                              new{N, T}(yield_through, yield_back, spawn_through, spawn_back)
end

SecEmissionFilm(p::AbstractParticle{N, T},
                yield_through::Function, yield_back::Function,
                spawn_through::Function, spawn_back::Function) where {N, T<:Number} =
                    SecEmissionFilm{N, T}(yield_through, yield_back, spawn_through, spawn_back)

function collision!(particle::AbstractParticle{1, T},
                    time::T,
                    intersection::Intersection{1, T},
                    collision::SecEmissionFilm{1, T}) where T<:Number
    speed = particle.speed
    normal_back = intersection.normal
    normal_through = -intersection.normal
    partnum_through = collision.yield_through(speed, 0)
    partnum_back = collision.yield_back(speed, 0)
    particle.timeshift = time - abs((intersection.point[1] -
                                     particle.position[1]) / particle.speed[1])
    particle.position .= MPoint(intersection.point)
    if partnum_through == 0 & partnum_back == 0
        particle.speed .= typemax(T)  # drop particle
    else
        if partnum_through >= partnum_back
            particle.speed .= collision.spawn_through(speed, normal_through)
            partnum_through -= 1
        else
            particle.speed .= collision.spawn_back(speed, normal_back)
            partnum_back -= 1
        end
        partnum = partnum_through + partnum_back
        if partnum >= 1
            new_particles = Array{typeof(particle)}(undef, partnum)
            for i in 1:partnum_through
                new_particles[i] = deepcopy(particle)
                new_particles[i].speed .= collision.spawn_through(speed, normal_through)
            end
            for i in 1:partnum_back
                new_particles[i] = deepcopy(particle)
                new_particles[i].speed .= collision.spawn_back(speed, normal_back)
            end
            return new_particles
        end
    end
    return []
end

# >>>

# <<< Boundary

abstract type AbstractBoundary{N, T<:Number} end

abstract type AbstractBoundaryCache{N, T<:Number} end

function process(x::AnyPoint{N, T},
                 b::AbstractBoundary{N, T})::AbstractBoundaryCache where {N, T}
end

struct Segment{T} <: AbstractBoundary{1, T}
    left::SPoint{1, T}
    right::SPoint{1, T}
    function Segment(x::AnyPoint{1, T}, y::AnyPoint{1, T}) where T<:Number
        sorted = sort([SPoint(x), SPoint(y)])
        new{T}(sorted[1], sorted[2])
    end
end

struct SegmentCache{T} <: AbstractBoundaryCache{1, T}
    point::SPoint{1, T}
    segment::Segment{T}
    inside::Bool
    SegmentCache{T}(x::AnyPoint{1, T}, y::Segment{T}, z::Bool) where T<:Number =
        new{T}(SPoint(x), y, z)
end

SegmentCache(x::AnyPoint{1, T}, y::Segment{T}, z::Bool) where T<:Number =
    SegmentCache{T}(x, y, z)

function process(x::AnyPoint{1, T},
                 b::Segment{T})::SegmentCache where T<:Number
    rv = b.left <= x <= b.right
    return SegmentCache(x, b, rv)
end

function process(x::AbstractParticle{1, T},
                 b::Segment{T})::SegmentCache where T<:Number
    return process(x.position, b)
end

function intersection(x::SegmentCache{T},
                      y::SegmentCache{T})::Union{Intersection{1, T}, Nothing} where T
    if x.segment !== y.segment
        error("Points belong to different geometries")
    end
    sorted = sort([x, y], by=x->x.point)
    x, y = sorted[1], sorted[2]
    if x.inside ⊻ y.inside
        if x.inside
            return Intersection(x.segment.right, SPoint(convert(T, -1)))
        else
            return Intersection(y.segment.left, SPoint(convert(T, 1)))
        end
    else
        return nothing
    end
end

struct PointCache{T} <: AbstractBoundaryCache{1, T}
    point::SPoint{1, T}
    origin::SPoint{1, T}
    normal::SPoint{1, T}
    PointCache{T}(x::AnyPoint{1, T}, y::AnyPoint{1, T}) where T<:Number =
        new{T}(SPoint(x), SPoint(y), SPoint(convert(T, x > y ? 1 : -1)))
end

PointCache(x::AnyPoint{1, T}, y::AnyPoint{1, T}) where T<:Number =
    PointCache{T}(x, y)

function process(x::AnyPoint{1, T},
                 y::AnyPoint{1, T})::PointCache where T<:Number
    return PointCache(x, y)
end

function process(x::AbstractParticle{1, T},
                 y::AnyPoint{1, T})::PointCache where T<:Number
    return process(x.position, y)
end

function intersection(x::PointCache{T},
                      y::PointCache{T})::Union{Intersection{1, T}, Nothing} where T
    if x.origin != y.origin
        error("Points belong to different geometries")
    end
    if x.normal != y.normal
        return Intersection(x.origin, x.normal)
    else
        return nothing
    end
end

# >>>

end # end of module EMgeometry

# Local Variables:
# vimish-fold-marks: ("<<<" . ">>>")
# End:
