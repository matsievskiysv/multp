using Test
using EMgeometry

@testset "geometry" begin

    # <<< Types

    @testset "types" begin
        @test eltype(SPoint(1)) == Int
        @test eltype(SPoint(1.0, 1.0)) <: AbstractFloat
        @test eltype(SPoint(1.0, 1)) <: AbstractFloat
        @test eltype(SPoint(1.0, 1, 1.0im)) <: Complex{T} where {T<:AbstractFloat}
        @test_throws DimensionMismatch SPoint(1, 1, 1, 1)
        @test eltype(MPoint(1)) == Int
        @test eltype(MPoint(1.0, 1.0)) <: AbstractFloat
        @test eltype(MPoint(1.0, 1)) <: AbstractFloat
        @test eltype(MPoint(1, 1, 1+1im)) <: Complex{Int}
        @test_throws DimensionMismatch MPoint(1, 1, 1, 1)
        @test typeof(MPoint(SPoint(1))) == MPoint{1, Int}
        @test typeof(MPoint(SPoint(1, 2.0))) <: MPoint{2, T} where {T<:AbstractFloat}
        @test typeof(MPoint(SPoint(1, 2.0, 3im))) <: MPoint{3, Complex{T}} where {T<:AbstractFloat}
        @test typeof(SPoint(MPoint(1))) == SPoint{1, Int}
        @test typeof(SPoint(MPoint(1, 2.0))) <: SPoint{2, T} where {T<:AbstractFloat}
        @test typeof(SPoint(MPoint(1, 2, 3im))) <: SPoint{3, Complex{Int}}

        @test typeof(convert(SPoint, MPoint(1))) == SPoint{1, Int}
        @test typeof(convert(SPoint, MPoint(1, 1))) == SPoint{2, Int}
        @test typeof(convert(SPoint, MPoint(1, 1, 2im))) == SPoint{3, Complex{Int}}
        @test typeof(convert(MPoint, SPoint(1))) == MPoint{1, Int}
        @test typeof(convert(MPoint, SPoint(1, 1))) == MPoint{2, Int}
        @test typeof(convert(MPoint, SPoint(1, 1, 2im))) == MPoint{3, Complex{Int}}

        point = MPoint(1)
        @test point[1] == 1
        point[1] = 2
        @test point[1] == 2
        point = MPoint(1, 0.2)
        @test point[1] == 1.0
        @test point[2] == 0.2
        point[1] = 2
        point[2] = 3
        @test point[1] == 2.0
        @test point[2] == 3.0
        point = MPoint(1, 0.2, 3)
        @test point[1] == 1.0
        @test point[2] == 0.2
        @test point[3] == 3.0
        point[1] = 2
        point[2] = 3
        point[3] = 1.1
        @test point[1] == 2.0
        @test point[2] == 3.0
        @test point[3] == 1.1

        point = SPoint(1)
        @test point[1] == 1
        @test_throws ErrorException point[1] = 2
        point = SPoint(1, 0.2)
        @test point[1] == 1.0
        @test point[2] == 0.2
        @test_throws ErrorException point[1] = 2
        @test_throws ErrorException point[2] = 3
        point = SPoint(1, 0.2, 3)
        @test point[1] == 1.0
        @test point[2] == 0.2
        @test point[3] == 3.0
        @test_throws ErrorException point[1] = 2
        @test_throws ErrorException point[2] = 3
        @test_throws ErrorException point[3] = 1.1

        @test_throws MethodError SField(1, 1)
        @test typeof(SField(SPoint(1), SPoint(1))) == SField{1, 1, Int, Int}
        @test eltype(SField(SPoint(1), SPoint(1))) == (Int, Int)
        @test eltype(SField(SPoint(1), SPoint(1im))) == (Int, Complex{Int})
        @test typeof(SField(SPoint(1), MPoint(1))) == SField{1, 1, Int, Int}
        @test typeof(MField(MPoint(1), SPoint(1im))) == MField{1, 1, Int, Complex{Int}}
        @test typeof(MField(MPoint(1.0), SPoint(1im))) <: MField{1, 1, T, Complex{Int}} where {T<:AbstractFloat}
        @test typeof(MField(MPoint(1.0), SPoint(1im)).point) <: MPoint
        @test typeof(SField(MPoint(1.0), SPoint(1im)).point) <: SPoint

        @test typeof(SField(MField(MPoint(1), SPoint(1)))) == SField{1, 1, Int, Int}
        @test typeof(MField(SField(MPoint(1), SPoint(1)))) == MField{1, 1, Int, Int}

        @test typeof(convert(SField, MField(MPoint(1), SPoint(1)))) == SField{1, 1, Int, Int}
        @test typeof(convert(MField, SField(MPoint(1), SPoint(1)))) == MField{1, 1, Int, Int}

        field = MField(SPoint(1), SPoint(1))
        @test field[1][1] == 1
        @test field[2][1] == 1
        field[1][1] = 2
        field[2][1] = 3
        @test field[1][1] == 2
        @test field[2][1] == 3
        field.field[1] = 4
        field.point[1] = 5
        @test field[1][1] == 4
        @test field[2][1] == 5
        @test_throws DomainError MField(SPoint(1, 2), SPoint(1))
        field = MField(SPoint(1.0), SPoint(1.0, 1.1))
        @test field[1][1] == 1.0
        @test field[2][1] == 1.0
        @test field[2][2] == 1.1
        field[1][1] = 2
        field[2][1] = 3
        field[2][2] = 4
        @test field[1][1] == 2.0
        @test_throws BoundsError field[1][2] == 2.0
        @test field[2][1] == 3.0
        @test field[2][2] == 4.0
        @test_throws BoundsError field[1][3] == 2.0
        field = MField(SPoint(1.0, 2), SPoint(1.0, 1.1))
        @test field[1][2] == 2.0
        field[1][2] = 3
        @test field[1][2] == 3.0
        field = MField(SPoint(2.0), SPoint(1.0, 1.1, 1.2))
        @test field[1][1] == 2.0
        field[1][1] = 3
        @test field[1][1] == 3.0
        field = MField(SPoint(2.0, 3.0), SPoint(1.0, 1.1, 1.2))
        @test field[1][1] == 2.0
        field[1][1] = 3
        @test field[1][1] == 3.0
        field = MField(SPoint(2.0, 4.0, 5.5), SPoint(1.0, 1.1, 1.2))
        @test field[1][1] == 2.0
        field[1][1] = 3
        @test field[1][1] == 3.0
        @test_throws DimensionMismatch MField(MPoint(1, 2, 3, 4), MPoint(1, 2, 3))

        field = SField(MPoint(1), MPoint(1))
        @test field[1][1] == 1
        @test field[2][1] == 1
        @test_throws ErrorException field[1][1] = 2
        @test_throws DomainError SField(SPoint(1, 2), SPoint(1))
        field = SField(MPoint(1), MPoint(1, 2))
        @test field[1][1] == 1
        @test_throws ErrorException field[1][1] = 2
        field = SField(MPoint(1, 2), MPoint(1, 2))
        @test field[1][1] == 1
        @test_throws ErrorException field[1][1] = 2
        field = SField(MPoint(1), MPoint(1, 2, 3))
        @test field[1][1] == 1
        @test_throws ErrorException field[1][1] = 2
        field = SField(MPoint(1, 2), MPoint(1, 2, 3))
        @test field[1][1] == 1
        @test_throws ErrorException field[1][1] = 2
        field = SField(MPoint(1, 2, 3), MPoint(1, 2, 3))
        @test field[1][1] == 1
        @test_throws ErrorException field[1][1] = 2
        @test_throws DimensionMismatch SField(MPoint(1, 2, 3, 4), MPoint(1, 2, 3))

    end

    # >>>

    # <<< Basic methods

    @testset "arithmetic & logical operators" begin
        @test SPoint(1.1) == SPoint(1.1)
        @test SPoint(1.1) == MPoint(1.1)

        # 1D
        @test SPoint(1.0) == SPoint(1)
        @test SPoint(1.1) < MPoint(1.2)
        @test MPoint(1.1) <= SPoint(1.2)
        @test SPoint(5) > SPoint(1.2)

        # 2D
        @test MPoint(0, 0) == SPoint(0, 0)
        @test SPoint(1, 0) > SPoint(0, 0)
        @test SPoint(0, 1) >= MPoint(0, 0)
        @test SPoint(1, 0) < SPoint(0, 1)
        @test SPoint(1, 1) >= SPoint(0, 1)

        # 3D
        @test MPoint(0, 0, 0) == SPoint(0, 0, 0)
        @test MPoint(1, 0, 0) > SPoint(0, 0, 0)
        @test MPoint(1, 0, 0) <= SPoint(0, 1, 0)
        @test MPoint(0, 1, 0) > SPoint(1, 0, 0)
        @test MPoint(0, 1, 0) >= SPoint(1, 0, 0)
        @test MPoint(0, 1, 0) < SPoint(1, 1, 0)
        @test MPoint(1, 1, 0) == SPoint(1, 1, 0)
        @test MPoint(0, 0, 1) > SPoint(1, 1, 0)
        @test MPoint(1, 0, 1) < SPoint(0, 1, 1)
        @test MPoint(1, 1, 1) > SPoint(0, 1, 1)

        # 1D
        @test MField(SPoint(1), SPoint(2)) < MField(SPoint(2), SPoint(1))
        @test SField(SPoint(2), SPoint(2)) <= MField(SPoint(2), SPoint(1))
        @test (MField(SPoint(1), SPoint(4)) + 1).field[1] == 2
        @test (MField(SPoint(1), SPoint(4)) + 1).point[1] == 4
        @test_throws InexactError (MField(SPoint(2), SPoint(4)) - 1.1).field[1] == 0.9
        @test (SField(SPoint(2), SPoint(4)) - 1.1).field[1] ≈ 0.9

        @test (MField(SPoint(2), SPoint(4)) + MField(SPoint(2), SPoint(4))).field[1] == 4
        @test (MField(SPoint(2), SPoint(4)) + SField(SPoint(2), SPoint(1))).point[1] == 4
        @test (MField(SPoint(3), SPoint(4)) - MField(SPoint(3), SPoint(1))).field[1] == 0

        # 2D
        @test MField(SPoint(1, 0), SPoint(20, 33)) < MField(SPoint(0, 2), SPoint(1,3))
        @test MField(SPoint(1, 2), SPoint(22, 34)) <= SField(SPoint(2, 2), SPoint(1,3))
        @test (MField(SPoint(1, 2), SPoint(4, 5)) + 2).field == MPoint(3, 4)
        @test (SField(SPoint(1, 2), SPoint(4, 5)) + 2).point == SPoint(4, 5)
        @test (SField(SPoint(1, 2), SPoint(4, 5)) - 2).field == SPoint(-1, 0)
        @test (MField(SPoint(2, 3), SPoint(4, 5)) + SField(SPoint(1, 2), SPoint(4, 5))).field == MPoint(3, 5)
        @test (SField(SPoint(2, 3), SPoint(4, 5)) - MField(SPoint(1, 2), SPoint(4, 5))).field == MPoint(1, 1)

        # 3D
        @test MField(SPoint(1, 2, 3), SPoint(20, 33, 4)) < SField(SPoint(1, 2, 3.1), SPoint(2, 3, 4))
        @test MField(SPoint(1, 2, 3.1), SPoint(20, 33, 4)) <= SField(SPoint(1, 2, 3.1), SPoint(2, 3, 4))
        @test (MField(SPoint(1, 2, 3), SPoint(4, 5, 6)) + 2).field == MPoint(3, 4, 5)
        @test (SField(SPoint(1, 2, 3), SPoint(4, 5, 6)) + 2).point == SPoint(4, 5, 6)
        @test (SField(SPoint(1, 2, 3), SPoint(4, 5, 6)) - 2).field == SPoint(-1, 0, 1)
        @test (MField(SPoint(2, 3, 3), SPoint(4, 5, 6)) +
               SField(SPoint(1, 2, 3), SPoint(4, 5, 1))).field == MPoint(3, 5, 6)
        @test (SField(SPoint(2, 3, 3), SPoint(4, 5, 6)) -
               MField(SPoint(1, 2, 3), SPoint(4, 5, 1))).field == MPoint(1, 1, 0)

    end

    @testset "vector rotation" begin
        # 1D
        @test rotate(SPoint(1), 0) ≈ SPoint(1)
        @test rotate(SPoint(1), π) ≈ MPoint(-1)
        @test isapprox(rotate(SPoint(1), π/2), SPoint(0), atol=1e-9)
        @test_throws MethodError rotate!(SPoint(1), 0)
        p = MPoint(1.0)
        @test p == MPoint(1.0)
        rotate!(p, 0)
        @test p == MPoint(1.0)
        rotate!(p, π)
        @test p == MPoint(-1.0)
        rotate!(p, π/2)
        @test isapprox(p, SPoint(0), atol=1e-9)

        # 2D
        @test rotate(SPoint(1.0, 0.0), 0) ≈ SPoint(1.0, 0.0)
        @test rotate(SPoint(1.0, 0.0), π/2) ≈ SPoint(0.0, 1.0)
        @test rotate(SPoint(1.0, 0.0), π) ≈ SPoint(-1.0, 0.0)
        @test rotate(SPoint(1.0, 0.0), 3π/2) ≈ SPoint(0.0, -1.0)
        @test rotate(SPoint(1.0, 0.0), π/4) ≈ SPoint(√2/2, √2/2)
        @test rotate(MPoint(1.0, 0.0), 0) ≈ MPoint(1.0, 0.0)
        @test rotate(MPoint(1.0, 0.0), π/2) ≈ MPoint(0.0, 1.0)
        @test rotate(MPoint(1.0, 0.0), π) ≈ MPoint(-1.0, 0.0)
        @test rotate(MPoint(1.0, 0.0), 3π/2) ≈ MPoint(0.0, -1.0)
        @test rotate(MPoint(1.0, 0.0), π/4) ≈ MPoint(√2/2, √2/2)
        p = MPoint(1.0, 0.0)
        rotate!(p, 0)
        @test p ≈ MPoint(1.0, 0.0)
        rotate!(p, π)
        @test p ≈ MPoint(-1.0, 0.0)
        rotate!(p, π/2)
        @test p ≈ MPoint(0.0, -1.0)
        rotate!(p, π/4)
        @test p ≈ MPoint(√2/2, -√2/2)

        # 3D
        @test rotate(SPoint(1.0, 0.0, 0.0)) ≈ SPoint(1.0, 0.0, 0.0)
        @test rotate(SPoint(1.0, 0.0, 0.0), α=π) ≈ SPoint(1.0, 0.0, 0.0)
        @test rotate(MPoint(0.0, 1.0, 0.0), α=π) ≈ MPoint(0.0, -1.0, 0.0)
        @test rotate(SPoint(0.0, 0.0, 1.0), α=π) ≈ SPoint(0.0, 0.0, -1.0)
        @test rotate(MPoint(1.0, 0.0, 0.0), α=π/2) ≈ MPoint(1.0, 0.0, 0.0)
        @test rotate(MPoint(0.0, 1.0, 0.0), α=π/2) ≈ MPoint(0.0, 0.0, 1.0)
        @test rotate(SPoint(0.0, 0.0, 1.0), α=π/2) ≈ SPoint(0.0, -1.0, 0.0)
        @test rotate(SPoint(1.0, 0.0, 0.0), β=π) ≈ SPoint(-1.0, 0.0, 0.0)
        @test rotate(MPoint(0.0, 1.0, 0.0), β=π) ≈ MPoint(0.0, 1.0, 0.0)
        @test rotate(SPoint(0.0, 0.0, 1.0), β=π) ≈ SPoint(0.0, 0.0, -1.0)
        @test rotate(MPoint(1.0, 0.0, 0.0), β=π/2) ≈ MPoint(0.0, 0.0, -1.0)
        @test rotate(MPoint(0.0, 1.0, 0.0), β=π/2) ≈ MPoint(0.0, 1.0, 0.0)
        @test rotate(SPoint(0.0, 0.0, 1.0), β=π/2) ≈ SPoint(1.0, 0.0, 0.0)
        @test rotate(SPoint(1.0, 0.0, 0.0), γ=π) ≈ SPoint(-1.0, 0.0, 0.0)
        @test rotate(MPoint(0.0, 1.0, 0.0), γ=π) ≈ MPoint(0.0, -1.0, 0.0)
        @test rotate(SPoint(0.0, 0.0, 1.0), γ=π) ≈ SPoint(0.0, 0.0, 1.0)
        @test rotate(MPoint(1.0, 0.0, 0.0), γ=π/2) ≈ MPoint(0.0, 1.0, 0.0)
        @test rotate(MPoint(0.0, 1.0, 0.0), γ=π/2) ≈ MPoint(-1.0, 0.0, 0.0)
        @test rotate(SPoint(0.0, 0.0, 1.0), γ=π/2) ≈ SPoint(0.0, 0.0, 1.0)

        p = MPoint(1.0, 0.0, 0.0)
        rotate!(p, γ=π/2)
        @test p ≈ MPoint(0.0, 1.0, 0.0)
        rotate!(p, α=π/2)
        @test p ≈ MPoint(0.0, 0.0, 1.0)
        rotate!(p, β=π/2)
        @test p ≈ MPoint(1.0, 0.0, 0.0)

        # 1D
        @test rotate(SField(SPoint(1.0), SPoint(1.0)), 0.0) ≈ SField(SPoint(1.0), SPoint(1.0))
        @test rotate(SField(SPoint(1.0), SPoint(1.0)), π) ≈ SField(SPoint(1.0), SPoint(0.0))
        @test rotate(MField(MPoint(1.0), MPoint(1.0)), π, field=true) ≈ MField(MPoint(0.0), MPoint(1.0))
        @test_throws MethodError rotate!(SField(SPoint(1), SPoint(2)), 0)

        p = MField(SPoint(1), SPoint(1))
        rotate!(p, π)
        @test p.point ≈ SPoint(-1)
        @test p.field ≈ SPoint(1)
        rotate!(p, π, field=true)
        @test p.point ≈ SPoint(-1)
        @test p.field ≈ SPoint(-1)

        # 2D
        @test rotate(SField(SPoint(1.0, 0.0), SPoint(1.0, 0.0)), 0) ≈ SField(SPoint(1.0, 0.0), SPoint(1.0, 0.0))
        @test rotate(MField(SPoint(1.0, 0.0), SPoint(1.0, 0.0)), π) ≈ MField(SPoint(1.0, 0.0), SPoint(-1.0, 0.0))
        @test rotate(MField(SPoint(1.0, 0.0), SPoint(1.0, 0.0)), π/2) ≈ MField(SPoint(1.0, 0.0), SPoint(0.0, 1.0))
        @test rotate(SField(SPoint(1.0, 0.0), SPoint(1.0, 0.0)), 0, field=true) ≈
            SField(SPoint(1.0, 0.0), SPoint(1.0, 0.0))
        @test rotate(MField(SPoint(1.0, 0.0), SPoint(1.0, 0.0)), π, field=true) ≈
            MField(SPoint(-1.0, 0.0), SPoint(1.0, 0.0))
        @test rotate(MField(SPoint(1.0, 0.0), SPoint(1.0, 0.0)), π/2, field=true) ≈
            MField(SPoint(0.0, 1.0), SPoint(1.0, 0.0))
        @test rotate(SField(SPoint(1.0), SPoint(1.0, 0.0)), 0, field=true) ≈
            SField(SPoint(1.0), SPoint(1.0, 0.0))
        @test rotate(MField(SPoint(1.0), SPoint(1.0, 0.0)), π, field=true) ≈
            MField(SPoint(-1.0), SPoint(1.0, 0.0))
        @test_throws MethodError rotate!(SField(SPoint(1, 2), SPoint(2, 3)), 0)

        p = MField(MPoint(1.0, 0.0), MPoint(1.0, 0.0))
        rotate!(p, 0)
        @test p.field ≈ MPoint(1.0, 0.0)
        @test p.point ≈ MPoint(1.0, 0.0)
        rotate!(p, π)
        @test p.field ≈ MPoint(1.0, 0.0)
        @test p.point ≈ MPoint(-1.0, 0.0)
        rotate!(p, π/2)
        @test p.field ≈ MPoint(1.0, 0.0)
        @test p.point ≈ MPoint(0.0, -1.0)
        rotate!(p, π, field=true)
        @test p.field ≈ MPoint(-1.0, 0.0)
        @test p.point ≈ MPoint(0.0, -1.0)
        rotate!(p, π/2, field=true)
        @test p.field ≈ MPoint(0.0, -1.0)
        @test p.point ≈ MPoint(0.0, -1.0)

        # 3D
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0))) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), α=π) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), α=π) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, -1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), α=π) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, -1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), α=π, field=true) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), α=π, field=true) ≈
            SField(SPoint(0.0, -1.0, 0.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), α=π, field=true) ≈
            SField(SPoint(0.0, 0.0, -1.0), SPoint(0.0, 0.0, 1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), α=π/2) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), α=π/2) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 0.0, 1.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), α=π/2) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, -1.0, 0.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), α=π/2, field=true) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), α=π/2, field=true) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), α=π/2, field=true) ≈
            SField(SPoint(0.0, -1.0, 0.0), SPoint(0.0, 0.0, 1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), β=π) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(-1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), β=π) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), β=π) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, -1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), β=π, field=true) ≈
            SField(SPoint(-1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), β=π, field=true) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), β=π, field=true) ≈
            SField(SPoint(0.0, 0.0, -1.0), SPoint(0.0, 0.0, 1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), β=π/2) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(0.0, 0.0, -1.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), β=π/2) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), β=π/2) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), β=π/2, field=true) ≈
            SField(SPoint(0.0, 0.0, -1.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), β=π/2, field=true) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), β=π/2, field=true) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(0.0, 0.0, 1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), γ=π) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(-1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), γ=π) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, -1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), γ=π) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), γ=π, field=true) ≈
            SField(SPoint(-1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), γ=π, field=true) ≈
            SField(SPoint(0.0, -1.0, 0.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), γ=π, field=true) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), γ=π/2) ≈
            SField(SPoint(1.0, 0.0, 0.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), γ=π/2) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(-1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), γ=π/2) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0))
        @test rotate(SField(SPoint(1.0, 0.0, 0.0), SPoint(1.0, 0.0, 0.0)), γ=π/2, field=true) ≈
            SField(SPoint(0.0, 1.0, 0.0), SPoint(1.0, 0.0, 0.0))
        @test rotate(SField(SPoint(0.0, 1.0, 0.0), SPoint(0.0, 1.0, 0.0)), γ=π/2, field=true) ≈
            SField(SPoint(-1.0, 0.0, 0.0), SPoint(0.0, 1.0, 0.0))
        @test rotate(SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0)), γ=π/2, field=true) ≈
            SField(SPoint(0.0, 0.0, 1.0), SPoint(0.0, 0.0, 1.0))
    end

    # >>>

    # <<< Mesh cells

    # @testset "mesh cells" begin
    #     # 1D
    #     @inferred Cell(SField(SPoint(0.1), SPoint(0.2)),
    #                    SField(SPoint(0.2), SPoint(0.3)))
    #     cell =  Cell(SField(SPoint(0.1), SPoint(0.5)),
    #                  SField(SPoint(0.2), SPoint(0.3)))
    #     @test cell[1][1] < cell[1][2]

    #     # 2D
    #     # @inferred Cell(SField(SPoint(0.1), SPoint(0.2, 0.1)),
    #     #                SField(SPoint(0.2), SPoint(0.1, 0.1)),
    #     #                SField(SPoint(0.3), SPoint(0.5, 2)),
    #     #                SField(SPoint(0.4), SPoint(0.3, 3.3)))
    #     # cell = Cell(SField(SPoint(0.1), SPoint(0.2, 0.1)),
    #     #             SField(SPoint(0.2), SPoint(0.1, 0.1)),
    #     #             SField(SPoint(0.3), SPoint(0.5, 2)),
    #     #             SField(SPoint(0.4), SPoint(0.3, 3.3)))
    #     # @test cell[1][1] < cell[1][2] < cell[1][3] < cell[1][4]
    # end

    #>>>

    # <<< Boundary

    @testset "boundaries" begin

        s = Segment(SPoint(2.0), MPoint(1.0))
        @test_throws MethodError Segment(SPoint(2.0, 2.2), MPoint(1.0, 1.1))
        @test typeof(s.left) <: SPoint
        @test typeof(s.right) <: SPoint
        @test typeof(process(MPoint(1.2), s)) <: SegmentCache{T} where T<:Number
        @test_throws MethodError process(MPoint(1), s).inside
        @test_throws MethodError process(MPoint(2), s).inside
        @test process(MPoint(1.2), s).inside
        @test !process(MPoint(0.2), s).inside
        @test !process(MPoint(2.2), s).inside
        @test !process(MPoint(2.2), s).inside
        p1 = process(MPoint(0.2), s)
        p2 = process(MPoint(2.2), s)
        @test p1.inside == p2.inside
        @test p1.segment === p2.segment
        @test intersection(p1, p2) == nothing
        p1 = process(MPoint(1.2), s)
        @test p1.inside != p2.inside
        @test typeof(intersection(p1, p2)) <: Intersection{1, T} where T<:AbstractFloat
        intr = intersection(p1, p2)
        @test intr.point == SPoint(2.0)
        @test intr.normal == SPoint(-1.0)
        p2 = process(MPoint(0.2), s)
        intr = intersection(p1, p2)
        @test intr.point == SPoint(1.0)
        @test intr.normal == SPoint(1.0)
    end

    # >>>

    # <<< Collision

    @testset "collisions" begin

        # send particle to the consuming wall

        s = Segment(SPoint(-1.0), MPoint(1.0))
        p = Pozitron(SPoint(0.0), SPoint(2.0))
        @test p.position[1] == 0.0
        @test p.speed[1] == 2.0
        @test p.timeshift == 0.0
        Δt = 1.0
        p1 = process(p, s)
        @test p1.inside == true
        new_pos = p.position + p.speed * Δt
        p2 = process(new_pos, s)
        @test p2.inside == false
        in = intersection(p1, p2)
        @test typeof(in) <: Intersection{N, T} where {N, T<:Number}
        @test collision!(p, Δt, in, Consume(p)) == []
        @test p.speed[1] == Inf
        @test p.position == MPoint(1.0)

        # send particle to the bouncing wall

        s = Segment(SPoint(-1.0), MPoint(1.0))
        p = Pozitron(SPoint(0.0), SPoint(2.0))
        Δt = 1.0
        p1 = process(p, s)
        new_pos = p.position + p.speed * Δt
        p2 = process(new_pos, s)
        in = intersection(p1, p2)
        @test collision!(p, Δt, in, Bounce(p)) == []
        @test p.position == SPoint(1.0)
        @test p.speed == SPoint(-2.0)
        @test p.timeshift ≈ 0.5
        p = Pozitron(SPoint(0.0), SPoint(-2.0))
        p1 = process(p, s)
        new_pos = p.position + p.speed * Δt
        p2 = process(new_pos, s)
        in = intersection(p1, p2)
        @test in.point == SPoint(-1.0)
        @test collision!(p, Δt, in, Bounce(p)) == []
        @test p.position == SPoint(-1.0)
        @test p.speed == SPoint(2.0)
        @test p.timeshift ≈ 0.5

        # send particle to the secondary emission wall

        s = Segment(SPoint(-1.0), MPoint(1.0))
        p = Pozitron(SPoint(0.0), SPoint(2.0))
        Δt = 1.0
        p1 = process(p, s)
        @test p1.inside == true
        new_pos = p.position + p.speed * Δt
        p2 = process(new_pos, s)
        @test p2.inside == false
        in = intersection(p1, p2)
        yield(speed, angle) = 0
        spawn(speed, normal) = Module(speed) * normal
        wall = SecEmissionWall(p, yield, spawn)
        @test collision!(p, Δt, in, wall) == []
        @test p.speed[1] == Inf
        @test p.position == MPoint(1.0)
        p = Pozitron(SPoint(0.0), SPoint(2.0))
        yield(speed, angle) = 1
        @test collision!(p, Δt, in, wall) == []
        @test p.position == MPoint(1.0)
        @test p.speed == MPoint(-2.0)
        @test p.timeshift == 0.5
        p = Pozitron(SPoint(0.0), SPoint(2.0))
        yield(speed, angle) = 2
        ps = collision!(p, Δt, in, wall)
        @test length(ps) == 1
        @test typeof(ps[1]) <: Pozitron{1, T} where {T<:Number}
        @test ps[1].position !== p.position
        @test ps[1].speed !== p.speed

        # send particle to the secondary emission film

        s = SPoint(2.0)
        p = Pozitron(SPoint(0.0), SPoint(3.0))
        Δt = 1.0
        p1 = process(p, s)
        @test p1.normal == SPoint(-1.0)
        new_pos = p.position + p.speed * Δt
        p2 = process(new_pos, s)
        @test p2.normal == SPoint(1.0)
        in = intersection(p1, p2)
        @test in != nothing
        yield_through(speed, angle) = 0
        yield_back(speed, angle) = 0
        spawn(speed, normal) = Module(speed) * normal
        w = SecEmissionFilm(p, yield_through, yield_back, spawn, spawn)
        @test collision!(p, Δt, in, w) == []
        @test p.position == MPoint(2.0)
        @test p.speed[1] == Inf
        p = Pozitron(SPoint(0.0), SPoint(3.0))
        yield_through(speed, angle) = 1
        yield_back(speed, angle) = 0
        @test collision!(p, Δt, in, w) == []
        @test p.position == MPoint(2.0)
        @test p.speed == MPoint(3.0)
    end

    # >>>

end

# Local Variables:
# vimish-fold-marks: ("<<<" . ">>>")
# End:
